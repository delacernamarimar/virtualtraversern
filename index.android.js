/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import { AppRegistry } from 'react-native';
import FluxRouter from './app/FluxRouter';
AppRegistry.registerComponent('virtualtraversern', () => FluxRouter);
