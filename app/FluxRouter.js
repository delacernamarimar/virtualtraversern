import React, {
    Component,
} from 'react';
import {
    StyleSheet,
    Text,
    View, AppState, BackAndroid, Platform, Dimensions
} from 'react-native';
import {
    Scene,
    Reducer,
    Router,
    Modal,
    Actions,
    ActionConst,
} from 'react-native-router-flux';
import TabIcon from './components/TabIcon';
import NavigationDrawer from './components/NavigationDrawer';
import VTRegistration from './components/VTRegistration';
import VTHomePage from './components/VTHomePage';
import SecondTab from './components/SecondTab';
import VTDateTransactions from './components/VTDateTransactions';
import VTProfileScreen from './components/VTProfileScreen';
import CheckSession from './components/CheckSession';
import VTLoginPage from './components/VTLoginPage';
import VTVideoGuide from './components/VTVideoGuide';
import VTVideoTourist from './components/VTVideoTourist';
import VTTouristPayment from './components/VTTouristPayment';
const localIP = '192.168.1.2';
const databaseServer = 'http://' + localIP + '/virtualtraverse/public';
const mqttServer = 'tcp://' + localIP + ':1883';
const webRTCServer = 'http://' + localIP + ':5000';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: 'transparent', justifyContent: 'center',
        alignItems: 'center',
    },
    tabBarStyle: {
        backgroundColor: '#ee09df',
    },
    tabBarSelectedItemStyle: {
        backgroundColor: 'white',
    },
});
const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        // console.log('ACTION:', action);
        return defaultReducer(state, action);
    };
};

// define this based on the styles/dimensions you use
const getSceneStyle = (/* NavigationSceneRendererProps */ props, computedProps) => {
    const style = {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null,
    };
    if (computedProps.isActive) {
        style.marginTop = computedProps.hideNavBar ? 0 : 64;
        style.marginBottom = computedProps.hideTabBar ? 0 : 50;
    }
    return style;
};

class Example extends Component {
    constructor() {
        super()
        this.state = {
            session: '',
            currentAppState: AppState.currentState, session: false, sessionUser: false
        }
    }

    // componentWillUnmount() {
    //     AppState.addEventListener('change', state =>
    //         console.log('consoleMyLogAppState changed to', state)
    //     )
    //     if (Platform.OS == "android" && listener == null) {
    //         listener = BackAndroid.addEventListener("hardwareBackPress", () => {
    //             console.log('consoleMyLoglog out')
    //         })
    //     }
    // }

    // componentWillMount() {
    //     this.checkSesssion()
    // }

    // componentWillMount() {
    //     setTimeout(() => {
    //         this.checkSession();
    //     }, 500);
    // }

    render() {

        return (
            <Router createReducer={reducerCreate} getSceneStyle={getSceneStyle}
                    databaseServer={databaseServer}
                    mqttServer={mqttServer} webRTCServer={webRTCServer}>
                <Scene key="modal" component={Modal}>
                    <Scene key="root" hideNavBar hideTabBar type={ActionConst.REFRESH}>
                        <Scene key="CheckSession" component={CheckSession} initial title="CheckSession"/>
                        <Scene key="VTLoginPage" component={VTLoginPage} title="Login" type={ActionConst.REPLACE}/>
                        <Scene key="VTRegistration" component={VTRegistration} title="Registration"/>
                        <Scene key="VTVideoGuide" component={VTVideoGuide} title="Video Guide" hideNavBar
                               hideTabBar/>
                        <Scene key="VTVideoTourist" component={VTVideoTourist} title="Video Tourist"
                               hideNavBar hideTabBar/>
                        <Scene key="NavigationDrawer" component={NavigationDrawer} type={ActionConst.REPLACE}>
                            <Scene key="main" tabs tabBarStyle={styles.tabBarStyle}
                                   tabBarSelectedItemStyle={styles.tabBarSelectedItemStyle}>
                                <Scene initial key="VTHomePage" component={VTHomePage} icon={TabIcon} hideNavBar
                                       title="Home"/>
                                <Scene key="SecondTab" component={SecondTab}
                                       title="Filtered" icon={TabIcon} hideNavBar/>
                                <Scene key="transact" icon={TabIcon} hideNavBar
                                       title="Transactions">
                                    <Scene initial key="VTDateTransactions" component={VTDateTransactions} hideNavBar
                                           title="Transactions" />
                                    <Scene key="VTTouristPayment" component={VTTouristPayment} hideNavBar
                                           title="Payment" />
                                </Scene>
                                <Scene key="VTProfileScreen" component={VTProfileScreen} title="Profile" hideNavBar
                                       icon={TabIcon} type={ActionConst.REFRESH}/>
                            </Scene>
                        </Scene>
                    </Scene>
                </Scene>
            </Router>
        );

    }

    // checkSesssion() {
    //     fetch(myLocalServer + '/showsession', {
    //         method: 'get',
    //         dataType: 'json',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         }
    //     })
    //         .then((response) => {
    //             return response.json() // << This is the problem
    //         })
    //         .then((responseData) => { // responseData = undefined
    //             if (responseData.username != null) {
    //                 // username = responseData.username + responseData.password
    //                 // myData = {"username": responseData.username, "password": responseData.password};
    //                 this.setState({type: responseData.type})
    //                 fetch(myLocalServer + '/query/' + responseData.username + '/' + responseData.password, {
    //                     method: 'get',
    //                     dataType: 'json',
    //                     headers: {
    //                         'Accept': 'application/json',
    //                         'Content-Type': 'application/json'
    //                     }
    //                 })
    //                     .then((response) => {
    //                         return response.json() // << This is the problem
    //                     })
    //                     .then((responseData) => { // responseData = undefined
    //                         return responseData;
    //                     })
    //                     .then((data) => {
    //                         this.setState({myData: data, session: true})
    //                     });
    //             } else {
    //                 this.setState({session: false})
    //             }
    //         })
    // }
}
export default Example;
