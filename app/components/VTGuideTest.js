/**
 * Created by DelaCerna on 2/13/17.
 */
import React, {Component} from 'react';
import {AppRegistry, View, Button, ToastAndroid} from 'react-native';
import {Container, Content,} from 'native-base';
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        };
    }

    componentWillMount(){
    }

    render() {
        return (
            <Container>
                <Content>
                    <Button onPress={()=>this.logout()}
                            title="Guide"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                    />
                </Content>
            </Container>
        );
    }

    logout() {
        console.log('clicked logout')
        fetch('http://192.168.1.5/virtualtraverse/public/stopsession', {
            method: 'get',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        ToastAndroid.show('You\'re logged out', ToastAndroid.SHORT);
    }
};