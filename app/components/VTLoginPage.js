import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    StyleSheet,
    TextInput,
    Dimensions,
    ToastAndroid,
    Alert,
    Platform,
    BackAndroid, TouchableWithoutFeedback, Image
} from 'react-native';

import {Container, Content, Item, Button, Label, Form, Input, Text} from 'native-base';
import {Actions} from "react-native-router-flux";
const {width, height} = Dimensions.get('window');
import LoadingScreen from './LoadingScreen';

//haskey
//Tfl/F40RWOvWFowWuR7nj1P52yw=

export default class extends Component {
    constructor() {
        super()
        this.state = {
            session: 0,
            username: '',
            password: '',
            type: '',
            call: true,
            sessionUserData: '',
            loading: false, disableLogin: false, fbUser: '', name: '', email: '', text: false
        }
    }

    render() {
        var logo = require('./pictures/vt_logo.png');
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content>

                    {this.state.loading ?
                        <LoadingScreen/>
                        : null }
                    <View style={styles.mainContainer}>
                        <View style={{alignItems:'center',justifyContent:'center', height:height*.4}}>
                            <Image
                                style={{resizeMode:'stretch'}}
                                source={logo}
                            />
                        </View>
                        <View style={{margin:5}}>
                            <Button block bordered  style={{margin:5}} onPress={Actions.VTRegistration}>
                                <Text>SIGN-UP</Text>
                            </Button>
                        </View>
                        <Form>
                            <Item floatingLabel>
                                <Label>Username</Label>
                                <Input style={{width:width*.8}}
                                       onChangeText={(username) => this.setState({username})}
                                       value={this.state.username}/>
                            </Item>
                            <Item floatingLabel last>
                                <Label>Password</Label>
                                <Input style={{width:width*.8}} secureTextEntry = {true}
                                       onChangeText={(password) => this.setState({password})}
                                       value={this.state.password}/>
                            </Item>
                        </Form>

                        <View style={{margin:5}}>
                            <Button block bordered style={{margin:5}} disabled={this.state.disableLogin}
                                    onPress={()=>this.logInPost(this.state.username, this.state.password)}><Text>LOG-IN</Text></Button>
                        </View>
                        <Text style={{color:'black', textAlign:'center', margin:5}}>or</Text>

                            {/*<Button block><Text>FACEBOOK</Text></Button>*/}
                            {/*<FBLogin*/}
                            {/*loginBehavior={LoginBehavior[Platform.OS]}*/}
                            {/*buttonView={<FBLoginView />}*/}
                            {/*ref={(fbLogin) => { this.fbLogin = fbLogin }}*/}
                            {/*permissions={["email","user_friends"]}*/}
                            {/*onLogin={function(e){ToastAndroid.show('onLogin:'+ e.credentials.userId, ToastAndroid.SHORT)}}*/}
                            {/*onLoginFound={function(e){console.log('onLoginFound:'+JSON.stringify(e.credentials))}}*/}
                            {/*onLoginNotFound={function(e){console.log('onLoginNotFound:'+JSON.stringify(e.credentials))}}*/}
                            {/*onPermissionsMissing={function(e){console.log('onPermissionsMissing'+JSON.stringify(e.credentials))}}*/}
                            {/*/>*/}

                        <Button bordered style={{margin:5}} block><Text>FACEBOOK</Text></Button>

                            <Button bordered style={{margin:5}} block><Text>GOOGLE+</Text></Button>

                        {/*<Text style={{color:'black', textAlign:'center', margin:5}}>forgot password</Text>*/}
                    </View>

                </Content>
            </Container>
        );
    }

    // loginFB(e) {
    //
    //     var api = `https://graph.facebook.com/v2.3/${e.credentials.userId}?fields=name,email&access_token=${e.credentials.token}`;
    //     fetch(api, {
    //         method: 'GET',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //         }
    //     }).then((response) => {
    //         return response.json() // << This is the problem
    //     })
    //         .then((responseData) => { // responseData = undefined
    //             this.setState({fbUser: responseData})
    //
    //             // return responseData;
    //         })
    // }

    logInPost(username, password) {
        this.setState({loading: true, disableLogin: true, text: true})
        if (username == '' || password == '') {
            ToastAndroid.show('Please input all fields!', ToastAndroid.SHORT);

        } else
            fetch(this.props.databaseServer + '/post/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                })
            }).then((response) => {
                return response.json() // << This is the problem
            }).then((responseData) => { // responseData = undefined
                    this.setState({sessionUserData: responseData})
                    if (responseData == null) {
                        this.wrongUsernamePassword()
                    } else {
                        if (this.state.sessionUserData.UserTypeNo == 1
                            || this.state.sessionUserData.UserTypeNo == 2) {
                            Actions.NavigationDrawer({sessionUserData: responseData})
                        } else {
                            if (this.state.sessionUserData.UserTypeNo == 3)
                                this.adminView()
                            else {
                                if (responseData.result == false)
                                    Alert.alert(
                                        'Error!',
                                        'User not found!',
                                        [
                                            {
                                                text: 'OK!', onPress: () => console.log("OK")
                                            },
                                        ],
                                        {cancelable: false}
                                    )
                            }
                        }
                    }
                }).catch(function (error) {
                Alert.alert(
                    'Error!',
                    'Can\'t connect to to server!\n Make sure you have internet connection.',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
        this.setState({loading: false, disableLogin: false, text: false})
    }

    wrongUsernamePassword() {
        Alert.alert(
            'Error!',
            'Username or password does not match!',
            [
                {
                    text: 'OK!', onPress: () => console.log('OK')
                },
            ],
            {cancelable: false}
        )
    }

    adminView() {
        Alert.alert(
            'Sorry!',
            'Admin mobile view is on development!',
            [

                // {text: 'CANCEL', onPress: () => console.log('cancel')},
                {
                    text: 'OK!', onPress: () => console.log('OK')
                },
            ],
            {cancelable: false}
        )
    }
};
const styles = StyleSheet.create({
    inputGroup: {
        backgroundColor: 'gray',
        margin: 5
    },
    mainContainer: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: 'white',
        padding: 10
    },
    centerAll: {
        alignItems: 'center', padding: 5, margin: 5
    },
    button: {
        padding: 5, margin: 5, width: width * .5
    }
});