/**
 * Created by DelaCerna on 2/8/17.
 */
/**
 * Created by DelaCerna on 1/25/17.
 */
import {
    View,
    StyleSheet,
    Modal, Picker, Dimensions, Text, Button
} from "react-native"
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
import {Container, Content} from 'native-base';
var {height, width} = Dimensions.get('window');
import VTListTourComments from './VTListTourComments';
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItem: undefined,
            selected1: 'key0',
            results: {
                items: [],
            },
        };
    }

    //noinspection JSAnnotator
    onValueChange(value: string) {
        this.setState({
            selected1: value,
        });
    }

    render() {

        return (
            <Container style={{backgroundColor: 'black',alignItems:'center', padding:5, justifyContent:'center'}}>
                <Content>
                    <View style={{width:width*.9,backgroundColor:'white'}}>
                        <Text style={{fontSize:30, fontWeight:'bold',padding:5}}>Marimar V. Dela Cerna</Text>
                        <Text style={{fontSize:20, fontWeight:'bold',padding:5}}>Ranked S</Text>
                        <View style={{padding:5}}>
                            <Text style={{}}>Address: Doña Cristina Subdivision Banawa Cebu City</Text>
                            <Text style={{}}>Contact#: 09430639369</Text>
                            <Text style={{}}>Email: delacernamarimar@gmail.com</Text>
                            <Text style={{}}>Date joined: 04-02-2016</Text>
                            <Text style={{}}>Video Examples</Text>
                            <Text style={{}}>"https://www.youtube.com/watch?v=9mkTkjyB0I8&t=387s"</Text>
                            <Text style={{}}>"https://www.youtube.com/watch?v=9mkTkjyB0I8&t=387s"</Text>
                            <Button title={'Hire now!'} onPress={()=>console.log('aw')}/>
                        </View>
                        <VTListTourComments/>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    inputGroup: {
        backgroundColor: 'gray',
        margin: 5
    },
    mainContainer: {
        height: height,
        alignItems: 'center',
        // backgroundColor: 'black',
        padding: 10, justifyContent: 'center'
    }
});
