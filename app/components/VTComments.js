/**
 * Created by DelaCerna on 1/25/17.
 */
import {
    View,
    StyleSheet,
    Modal, Picker, Dimensions, TouchableHighlight, Image
} from "react-native"
import {Button, Item, Text} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
import {Container, Content} from 'native-base';
var {height, width} = Dimensions.get('window');
export default class extends Component {

    render() {
        return (
            <View
                style={{backgroundColor:'white',margin: width *.0125, borderWidth: 1,  borderColor: "rgba(0,0,0,0.5)"}}>
                <Text>by:Mary Anne Chavez</Text>
                <Text style = {{fontWeight:'bold',fontSize:20, margin:5}}>“Bohol Tour Recommendation”</Text>
                <Text style = {{fontWeight:'bold',fontSize:12, margin:5}}>Reviewed October 5, 2009</Text>
                <Text>Had a great time in Bohol. Will return again. I will recommend the Firefly Tour in the river at night. It gave me goosebumps to see thousands of fireflies in 15 trees (300 fireflies per tree) as we went to the Loay and Loboc rivers in Bohol.
You should only take this tour when there is no storm or no strong winds, during a storm or strong winds, the fireflies are not there but we had good weather last night, October 4, 2009.
I will recommend you contact Ms Lillet who arranged our transfer in and all tour arrangements, their rates are half of the hotel and they have very professional/polite/friendly/safe service. HIghly recommended. I found Ms Lillet being recommended here also in Trip Advisor by a Taiwan based guest. Ms Lillet's number is +639173042385 and email angelberttours@yahoo.com.
                </Text>
                <Text style = {{fontWeight:'bold', margin:5}}>Rated:5/10</Text>
            </View>

        );
    }
}

