/**
 * Created by DelaCerna on 1/25/17.
 */
import {
    View,
    StyleSheet,
    Modal, Picker, Dimensions, TouchableHighlight, Image, Button
} from "react-native"
import { Item, Text} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
import {Container, Content} from 'native-base';
var {height, width} = Dimensions.get('window');
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItem: undefined,
            selected1: 'key0',
            results: {
                items: [],
            }, value: 'guide'
        };
    }

    render() {
        return (
            <View
                style={{backgroundColor:'white',flex:1, alignItems:'center',margin: width *.0125, borderWidth: 1,  borderColor: "rgba(0,0,0,0.5)"}}>
                <View
                    style={{ flexDirection:'row',
                    height: height * .2,justifyContent:'space-between'
                }}>
                    <View style={{width: width * .6,margin:width*.01,justifyContent:'space-between'}}>
                        <Text>{this.props.myData.Username}</Text>
                        <Text> Bohol City</Text>
                        <Text> Ranked: 5/10</Text>
                        {/*<Button title={'View details'} onPress={Actions.VTRatedVideoTours}/>*/}
                    </View>
                    <View style={{margin:width*.01}}>
                        <Text
                            style={{
                      color: 'black',
                      fontSize: 16,
                      fontWeight: 'normal',
                      fontFamily: 'Helvetica Neue', textAlign:'center'
                    }}>
                            {this.props.name}
                        </Text>
                        <Image
                            style={{
                            width:  width * .3 ,
                            height:  height * .15 ,
                        }}
                            resizeMode={"contain"}
                            source={{uri:'https://unsplash.it/600/400/?random'}}
                        />
                    </View>
                </View>
                <View style={{width:width*.4}}>
                    {/*<TouchableHighlight*/}
                    {/*activeOpacity={75 / 100}*/}
                    {/*underlayColor={"rgb(210,210,210)"}>*/}
                    <Text style={{textAlign:'center'}}>Review and Rating</Text>
                    {/*</TouchableHighlight>*/}
                </View>
            </View>

        );
    }
}

