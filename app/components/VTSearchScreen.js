/**
 * Created by DelaCerna on 2/13/17.
 */
import React, {Component} from 'react';
import {AppRegistry, View, ToastAndroid, Text, StyleSheet, Alert, Dimensions} from 'react-native';
import {Container, Content, Icon} from 'native-base';
import {Button, Header, Item, Input, Fab, List, ListItem, Spinner, Left, Right, Body, Title, Picker} from 'native-base';
import {Actions} from "react-native-router-flux";
import VTCardView from './VTCardViewTourist';
import CacheStore from 'react-native-cache-store';
const {width, height} = Dimensions.get('window');
let next_page_url, prev_page_url;
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searching: false, active: false, buttonWidth: 0, rankActive: false, searched: false,
            selectedItem: undefined,
            selected1: 'key1',
            results: {
                items: []
            }
        };
    }

    onValueChange(value: string) {
        this.setState({
            selected1: value
        });
    }

    render() {
        var counter = -1;

        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content>
                    {this.state.loading ?
                        <Spinner color='blue' style={{position:'absolute', zIndex:5, top:height*5}}/>
                        : null}
                    {this.state.searched ?
                        <View>
                            <Header style={{backgroundColor:'#ee09df'}}>
                                <Left>
                                    { prev_page_url == null ? null :
                                        <Button transparent onPress={()=>this.previousPage()}>
                                            <Icon name='ios-arrow-back'/>
                                        </Button>
                                    }
                                </Left>
                                <Body>
                                <Title>Page: {this.state.tours.current_page}</Title>
                                </Body>
                                <Right>
                                    {next_page_url == null ? null :
                                        <Button transparent onPress={()=>this.nextPage()}>
                                            <Icon name='ios-arrow-forward'/>
                                        </Button>
                                    }
                                </Right>
                            </Header>
                            <List style={{padding:5}} dataArray={this.state.tours.data} renderRow={(data) =>
                    <ListItem>
                        <VTCardView
                        databaseServer={this.props.databaseServer}
                        mqttServer={this.props.mqttServer}
                        tours = {this.state.tours.data}
                        data = {data}
                        counter = {counter=counter+1}/>
                    </ListItem>
                    }/></View>
                        :
                        null}
                </Content>
                <Fab
                    active={this.state.active}
                    direction="up"
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomLeft"
                    onPress={() => this.clickFab()}
                >
                    <Icon name="ios-search"/>
                    <Button style={{ width:this.state.buttonWidth, backgroundColor: '#34A34F' }}
                            onPress={()=>this.searchTour('rank')}>
                        <Text style={{fontSize:12}}>Rank</Text>
                    </Button>
                    <Button style={{ width:this.state.buttonWidth, backgroundColor: '#3B5998' }}
                            onPress={()=>this.searchTour('category')}>
                        <Text style={{fontSize:12}}>Category</Text>
                    </Button>
                    <Button style={{ width:this.state.buttonWidth, backgroundColor: '#DD5144' }}
                            onPress={()=>this.searchTour('destination')}>
                        <Text style={{fontSize:12}}>Destination</Text>
                    </Button>
                </Fab>
            </Container>
        );
    }

    clickFab() {
        if (this.state.active)
            this.setState({active: !this.state.active, buttonWidth: 0})
        else
            this.setState({active: !this.state.active, buttonWidth: width * .3})
    }

    searchTour(filter) {
        fetch(this.props.databaseServer + '/get/tours/tourist/filter/' + filter, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        }).then((responseData) => { // responseData = undefined
            CacheStore.set('tours', responseData);
            this.setState({tours: responseData, searched: true})
            next_page_url = this.state.tours.next_page_url;
            prev_page_url = this.state.tours.prev_page_url;
            this.clickFab()

        })
            .catch(function (error) {
                CacheStore.get('tours').then((value) => {
                    this.setState({tours: value})
                })
                throw error;
            });
    }

    previousPage() {
        this.setState({loading: true})
        fetch(prev_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                console.log(JSON.stringify(responseData))
                this.setState({tours: responseData})
            })
            .catch(function (error) {
                throw error;
            });
    }

    nextPage() {
        this.setState({loading: true})
        fetch(next_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                console.log(JSON.stringify(responseData))
                this.setState({tours: responseData})
            })
            .catch(function (error) {
                throw error;
            });
    }
};