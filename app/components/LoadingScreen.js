'use strict';

import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View, Dimensions } from 'react-native';
const {width, height} = Dimensions.get('window');
/**
 * Optional Flowtype state and timer types definition
 */
type State = { animating: boolean; };
type Timer = number;

export default class extends Component {
    /**
     * Optional Flowtype state and timer types
     */
    state: State;
    _timer: Timer;

    constructor(props) {
        super(props);
        this.state = {
            animating: true,
        };
    }

    componentDidMount() {
        this.setToggleTimeout();
    }

    componentWillUnmount() {
        clearTimeout(this._timer);
    }

    setToggleTimeout() {
        this._timer = setTimeout(() => {
            this.setState({animating: !this.state.animating});
            this.setToggleTimeout();
        }, 3000);
    }

    render() {
        return (
            <View style = {{flex:1,justifyContent:'center'}}>
            <ActivityIndicator
                animating={this.state.animating}
                style={[styles.centering, {height: 80}]}
                size="large"
            />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    centering: {zIndex: 1,
        top: height* .45,
        left:width*.45,
        position:'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
    gray: {
        backgroundColor: '#cccccc',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 8,
    },
});