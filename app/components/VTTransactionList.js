import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {Content, ListItem, Card, CardItem, Left, Body, Text, Button, Icon, Right, Thumbnail} from 'native-base';

export default class extends Component {
    render() {
        var transactionDate = this.props.data.Date;
        return (
            <Card style={{width:300}}>
                <CardItem>
                    <Left>
                        <Button
                                style={{justifyContent:'center',backgroundColor:'yellow',width: 60, height: 60, borderRadius: 30}}>
                            <Text
                                style={{alignSelf:'center', color: 'red', fontWeight: 'bold', fontSize: 25}}>
                                {this.props.sessionUserData.UserTypeNo == 1 ?
                                    this.props.data.Rank
                                    :
                                    this.props.sessionUserData.Rank
                                }
                            </Text>
                        </Button>
                        <Body>
                        <Text>{this.props.data.DestinationName}</Text>
                        <Text note>
                            {this.props.sessionUserData.UserTypeNo == 1 ?
                                this.props.data.Username
                                :
                                this.props.sessionUserData.Username
                            }
                        </Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image/>
                </CardItem>
                <CardItem content>
                    <View>
                        <Text>Tourist:
                            {this.props.sessionUserData.UserTypeNo == 2 ?
                                this.props.data.Username
                                :
                                this.props.sessionUserData.Username
                            }
                        </Text>
                        <Text>Durations: {this.props.data.Duration}</Text>
                        <Text>Paid: {this.props.data.TotalAmount}</Text>
                        <Text>Transaction Date: {this.props.data.Date}</Text>
                        <Text>Rate: {this.props.data.Rate}</Text>
                        <Text>Ban: {this.props.data.Ban}</Text>
                        <Text>Comment: {this.props.data.Review}</Text>
                    </View>
                </CardItem>
                <CardItem>
                    <Right>
                    </Right>
                </CardItem>
            </Card>
        );
    }
}