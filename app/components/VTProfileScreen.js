import React, {Component} from 'react';
import {AppRegistry, View, StyleSheet, TextInput, Dimensions, Text, ToastAndroid, Alert} from 'react-native';
import {
    Container,
    Content,
    Button,
    Form,
    Input,
    Label,
    Item,
    ListItem,
    Radio,
    Left,
    Header,
    Right,
    Body,
    Icon,
    Title
} from 'native-base';
import {Actions} from 'react-native-router-flux';
const {width, height} = Dimensions.get('window');
let Username;
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DOB: '', Username: '', Password: '', Email: '', FirstName: '', MiddleName: '', Gender: '',
            LastName: '', ContactNo: '', Address: '', editable: false, genderMale: true, genderFemale: false
            , userActive: false, userInactive: true
        };
    }

    componentDidMount() {
        this.setState({
            Gender: this.props.sessionUserData.Gender == 'M' ? this.setState({
                    genderMale: true,
                    genderFemale: false
                }) : this.setState({genderMale: false, genderFemale: true}),
            DOB: this.props.sessionUserData.Gender != null ? this.props.sessionUserData.DOB : '1990-30-12',
            Username: this.props.sessionUserData.Username,
            Password: this.props.sessionUserData.Password,
            Email: this.props.sessionUserData.Email,
            FirstName: this.props.sessionUserData.FirstName,
            MiddleName: this.props.sessionUserData.MiddleName,
            LastName: this.props.sessionUserData.LastName,
            ContactNo: this.props.sessionUserData.ContactNo,
            Address: this.props.sessionUserData.Address,
        })
        Username = this.props.sessionUserData.Username;
    }

    render() {
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content>
                    <Header style={{backgroundColor:'#ee09df'}}>
                        <Left>


                        </Left>
                        <Body>
                        <Title>Profile</Title>
                        </Body>
                        <Right>


                        </Right>
                    </Header>
                    {this.state.editable ?
                        <Form>
                            <Item stackedLabel>
                                <Label>Username</Label>
                                <Input style={{width:width*.8}}
                                       onChangeText={(Username) => this.setState({Username:Username})}
                                       value={this.state.Username}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Password</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(Password) => this.setState({Password:Password})}
                                       value={this.state.Password}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Email</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(Email) => this.setState({Email:Email})}
                                       value={this.state.Email}/>
                            </Item>

                            <Item stackedLabel>
                                <Label>First Name</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(FirstName) => this.setState({FirstName:FirstName})}
                                       value={this.state.FirstName}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Middle Name</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(MiddleName) => this.setState({MiddleName:MiddleName})}
                                       value={this.state.MiddleName}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Last Name</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(LastName) => this.setState({LastName:LastName})}
                                       value={this.state.LastName}/>
                            </Item>

                            <Item stackedLabel>
                                <Label>Contact</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(ContactNo) => this.setState({ContactNo:ContactNo})}
                                       value={this.state.ContactNo}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Address</Label>
                                <Input style={{width:width*.8}}

                                       onChangeText={(Address) => this.setState({Address:Address})}
                                       value={this.state.Address}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Birth of Date</Label>
                                <Input style={{width:width*.8}}
                                       onChangeText={(DOB) => this.setState({DOB:DOB})}
                                       value={this.state.DOB}/>
                            </Item>
                            <View style={{flexDirection:'row', alignItems:'center', width:width}}>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.genderMale}
                                               onPress={()=>this._onPressButton()}/>
                                    </Left>
                                    <Text>Male</Text>
                                </ListItem>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.genderFemale}
                                               onPress={()=>this._onPressButton()}/>
                                    </Left>
                                    <Text>Female</Text>
                                </ListItem>
                            </View>
                        </Form>
                        :
                        <Form>
                            <Item stackedLabel>
                                <Label>Username</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(Username) => this.setState({Username})}
                                       value={this.state.Username}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Password</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(Password) => this.setState({Password})}
                                       value={this.state.Password}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Email</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(Email) => this.setState({Email})}
                                       value={this.state.Email}/>
                            </Item>

                            <Item stackedLabel>
                                <Label>First Name</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(FirstName) => this.setState({FirstName})}
                                       value={this.state.FirstName}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Middle Name</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(MiddleName) => this.setState({MiddleName})}
                                       value={this.state.MiddleName}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Last Name</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(LastName) => this.setState({LastName})}
                                       value={this.state.LastName}/>
                            </Item>

                            <Item stackedLabel>
                                <Label>Contact</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(ContactNo) => this.setState({ContactNo})}
                                       value={this.state.ContactNo}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Address</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(Address) => this.setState({Address})}
                                       value={this.state.Address}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Birth of Date</Label>
                                <Input style={{width:width*.8}} disabled
                                       onChangeText={(DOB) => this.setState({DOB})}
                                       value={this.state.DOB}/>
                            </Item>
                            <View style={{flexDirection:'row', alignItems:'center', width:width}}>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.genderMale} disabled
                                               onPress={()=>this._onPressButton()}/>
                                    </Left>
                                    <Text>Male</Text>
                                </ListItem>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.genderFemale} disabled
                                               onPress={()=>this._onPressButton()}/>
                                    </Left>
                                    <Text>Female</Text>
                                </ListItem>
                            </View>
                        </Form>
                    }
                    {
                        !this.state.editable ?
                            <Button bordered info style={{margin:5}} block
                                    onPress={()=>this.setState({editable: true})}>
                                <Text>EDIT</Text>
                            </Button> :
                            <Button bordered info style={{margin:5}} block
                                    onPress={()=>
                                    this.checkUpdate()}>
                                <Text>DONE</Text>
                            </Button>
                    }

                    {/*<Button bordered info block style={{margin:5}} onPress={()=>this.logout()}>*/}
                    {/*<Text>Settings</Text>*/}
                    {/*</Button>*/}
                    {this.props.sessionUserData.UserTypeNo == 2 ?
                        <Form>
                            <View style={{flexDirection:'row', alignItems:'center', width:width}}>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.userActive}
                                               onPress={()=>this.changeStatus('A')}/>
                                    </Left>
                                    <Text>Active</Text>
                                </ListItem>
                                <ListItem style={{width:width*.3}}>
                                    <Left>
                                        <Radio selected={this.state.userInactive}
                                               onPress={()=>this.changeStatus('I')}/>
                                    </Left>
                                    <Text>Inactive</Text>
                                </ListItem>
                            </View>
                        </Form> : null}
                    <Button bordered info style={{margin:5}} block onPress={()=>this.logout()}>
                        <Text>LOGOUT</Text>
                    </Button>
                </Content>
            </Container>
        );
    }

    changeStatus(status) {
        fetch(this.props.databaseServer + '/post/user/update/status', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                UserNo: this.props.sessionUserData.UserNo,
                Status: status
            })
        }).then((response) => {
            return response.json() // << This is the problem
        }).then((responseData) => { // responseData = undefined
            if (status == 'A') {
                ToastAndroid.show('You\'re now active!', ToastAndroid.SHORT)
                this.setState({userActive: true, userInactive: false})
                this.available(this.state.Username, this.props.sessionUserData)
            }
            else {
                ToastAndroid.show('You\'re now inactive!', ToastAndroid.SHORT)
                this.setState({userActive: false, userInactive: true})
            }
        }).catch(function (error) {
            Alert.alert(
                'ERROR!',
                'Can\'t connect to to server!\n Status not changed!',
                [
                    {
                        text: 'OK!', onPress: () => console.log("OK")
                    },
                ],
                {cancelable: false}
            )
            throw error;
        });
    }

    _onPressButton() {
        if (this.state.genderMale) {
            this.setState({
                genderMale: false, genderFemale: true, Gender: 'F'
            })
        } else {
            this.setState({
                genderMale: true, genderFemale: false, Gender: 'M'
            })
        }
    }

    editDetails() {
        this.setState({editable: true})
    }

    saveDetails() {
        this.setState({editable: false})

    }

    checkUpdate() {
        Alert.alert(
            'CONFIRM!',
            'Update profile?',
            [
                {
                    text: 'CANCEL', onPress: () => this.setState({editable: false})
                },
                {
                    text: 'OK', onPress: () => this.updateUser()
                },
            ],
            {cancelable: false}
        )
    }

    updateUser() {

        fetch(this.props.databaseServer + '/get/post/user/update', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                UserNo: this.props.sessionUserData.UserNo,
                Username: this.state.Username,
                Password: this.state.Password,
                Email: this.state.Email,
                ContactNo: this.state.ContactNo,
                Address: this.state.Address,
                FirstName: this.state.FirstName,
                MiddleName: this.state.MiddleName,
                LastName: this.state.LastName,
                Gender: this.state.Gender,
                DOB: this.state.DOB
            })
        }).then((response) => {
            return response.json() // << This is the problem
        }).then((responseData) => { // responseData = undefined
            this.setState({editable: false})

        }).catch(function (error) {
            Alert.alert(
                'Error!',
                'Can\'t connect to to server!\n Make sure you have internet connection.\n Profile not saved!',
                [
                    {
                        text: 'OK!', onPress: () => console.log("OK")
                    },
                ],
                {cancelable: false}
            )
            throw error;
        });
        this.setState({loading: false, disableLogin: false, text: false})
    }


    logout() {
        console.log('clicked logout')
        fetch(this.props.databaseServer + '/stopsession', {
            method: 'get',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).then((response) => {
            Actions.VTLoginPage()
        })
            .catch(function (error) {
                Alert.alert(
                    'Error!',
                    'No internet connection!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
    }

    available(username, sessionUserData) {
        var mqtt = require('react-native-mqtt');
        /* create mqtt client */
        mqtt.createClient({
            // uri: 'mqtt://test.mosquitto.org:1883',
            uri: 'mqtt://test.mosquitto.org:1883',
            clientId: 'guide'
        }).then(function (client) {
            client.on('closed', function () {
                console.log('mqtt.event.closed');

            });

            client.on('error', function (msg) {
                console.log('mqtt.event.error', msg);

            });

            client.on('message', function (msg) {
                console.log('mqtt.event.message', msg);

                if (msg.data == 'cancel') {
                    ToastAndroid.show('Request has been cancelled!', ToastAndroid.SHORT);

                }

                if (msg.data == 'request')
                    Alert.alert(
                        'Accept Online Tour?',
                        'Someone want\'s you to be his/her online tour guide.',
                        [

                            {
                                text: 'NOT RIGHT NOW',
                                onPress: () => client.publish('/data/' + username, 'denied', 0, true)
                            },
                            {
                                text: 'ACCEPT!', onPress: () => client.publish('/data/' + username, 'granted', 0, true)}
                                ,
                        ],
                        {cancelable: false}
                    )
                // Actions.RCTWebRTCDemo({myData:this.props.myData})
                if(msg.data == 'startNow'){
                    client.disconnect()
                    Actions.VTVideoGuide({room:username})
                }

            })
                ;

                client.on('connect', function () {
                    console.log('connected');
                    client.subscribe('/data/' + username, 0);
                    client.publish('/data/' + username, 'active', 0, true);
                    // client.publish('/data', userNo, 0, false);
                });
                client.connect();
            }).catch(function (err) {
                console.log(err);
            });
        }


    };

    const
    styles = StyleSheet.create({
        inputGroup: {
            backgroundColor: 'gray',
            margin: 5
        },
        mainContainer: {
            flex: 1,
            alignItems: 'stretch',
            backgroundColor: 'black',
            padding: 10
        },
        centerAll: {
            alignItems: 'center', padding: 5, margin: 5
        },
        button: {
            padding: 5, margin: 5, width: width * .5
        },
        textInput: {
            height: 40, borderColor: 'gray', borderWidth: 1, padding: 5, margin: 5
        }
    });