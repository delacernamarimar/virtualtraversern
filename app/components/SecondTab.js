import React, {Component} from 'react';
import {AppRegistry, View, Button} from 'react-native';
import VTGuideDeletedTours from './VTGuideDeletedTours';
import VTSearchScreen from './VTSearchScreen';
export default class extends Component {
    render() {
        return (
            this.props.sessionUserData.UserTypeNo == 1 ?
                <VTSearchScreen
                    sessionUserData={this.props.sessionUserData} databaseServer={this.props.databaseServer}
                    mqttServer={this.props.mqttServer}
                /> :
                <VTGuideDeletedTours
                    sessionUserData={this.props.sessionUserData} databaseServer={this.props.databaseServer}
                    mqttServer={this.props.mqttServer}
                />

        );
    }
};