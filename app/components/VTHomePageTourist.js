/**
 * Created by DelaCerna on 2/13/17.
 */
import React, {Component} from 'react';
import {AppRegistry, View, Text, StyleSheet, Dimensions} from 'react-native';
import {
    Container,
    Content,
    ListItem,
    List,
    Fab,
    Spinner,
    Body,
    Right,
    Title,
    Icon,
    Button,
    Segment,
    Header,
    Left, Footer
} from 'native-base';
import VTCardViewTourist from './VTCardViewTourist';
let next_page_url, prev_page_url;
var {width, height} = Dimensions.get('window');
import CacheStore from 'react-native-cache-store';
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false, tours: '', active: false, loading: true
        };
    }

    componentDidMount() {
        fetch(this.props.databaseServer + '/get/tours/tourist', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        }).then((responseData) => { // responseData = undefined
            CacheStore.set('tours', responseData);
            this.setState({tours: responseData, loading: false})
        })
            .catch(function (error) {
                CacheStore.get('tours').then((value) => {
                    this.setState({tours: value, loading: true})
                })
                throw error;
            });

    }

    render() {
        var counter = -1;
        next_page_url = this.state.tours.next_page_url;
        prev_page_url = this.state.tours.prev_page_url;
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content style={{ margin:2.5}}>
                    <Header style={{backgroundColor:'#ee09df'}}>
                        <Left>
                            { prev_page_url == null ? null :
                                <Button transparent onPress={()=>this.previousPage()}>
                                    <Icon name='ios-arrow-back'/>
                                </Button>
                            }
                        </Left>
                        <Body>
                        <Title>Page: {this.state.tours.current_page}</Title>
                        </Body>
                        <Right>
                            {next_page_url == null ? null :
                                <Button transparent onPress={()=>this.nextPage()}>
                                    <Icon name='ios-arrow-forward'/>
                                </Button>
                            }
                        </Right>
                    </Header>
                    {this.state.loading ?
                        <Spinner color='blue' style={{position:'absolute', zIndex:5, top:height*5}}/>
                        :
                        <Content>
                            <List style={{padding:5}} dataArray={this.state.tours.data} renderRow={(data) =>
                    <ListItem>
                        <VTCardViewTourist
                        sessionUserData={this.props.sessionUserData}
                        databaseServer={this.props.databaseServer}
                        mqttServer={this.props.mqttServer}
                        tours = {this.state.tours.data}
                        data = {data}
                        counter = {counter=counter+1}/>
                    </ListItem>
                    }/>
                            <Footer style={{backgroundColor:'#ee09df'}} >
                                <Left>
                                    { prev_page_url == null ? null :
                                        <Button transparent onPress={()=>this.previousPage()}>
                                            <Icon name='ios-arrow-back'/>
                                        </Button>
                                    }
                                </Left>
                                <Body>
                                <Title>Page: {this.state.tours.current_page}</Title>
                                </Body>
                                <Right>
                                    {next_page_url == null ? null :
                                        <Button transparent onPress={()=>this.nextPage()}>
                                            <Icon name='ios-arrow-forward'/>
                                        </Button>
                                    }
                                </Right>
                            </Footer>
                        </Content>
                    }
                </Content>
            </Container>
        );
    }

    previousPage() {
        this.setState({loading: true})
        fetch(prev_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                // console.log(JSON.stringify(responseData))
                this.setState({tours: responseData, loading: false})
            })
            .catch(function (error) {
                throw error;
            });
    }

    nextPage() {
        this.setState({loading: true})
        fetch(next_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                // console.log(JSON.stringify(responseData))
                this.setState({tours: responseData, loading: false})
            })
            .catch(function (error) {
                throw error;
            });
    }
};

