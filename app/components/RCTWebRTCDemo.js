'use strict';

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    TextInput,
    ListView, Dimensions
} from 'react-native';
import {Container, Content, Button} from 'native-base';
const {width, height} = Dimensions.get('window');

import io from 'socket.io-client/dist/socket.io';

let socket;

import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
} from 'react-native-webrtc';

const configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};

const pcPeers = {};
let localStream;

function getLocalStream(isFront, callback) {
    MediaStreamTrack.getSources(sourceInfos => {
        console.log(sourceInfos);
        let videoSourceId;
        //noinspection JSAnnotator
        for (const i = 0; i < sourceInfos.length; i++) {
            const sourceInfo = sourceInfos[i];
            if (sourceInfo.kind == "video" && sourceInfo.facing == (isFront ? "front" : "back")) {
                videoSourceId = sourceInfo.id;
            }
        }
        getUserMedia({
            audio: true,
            video: {
                mandatory: {
                    // width: 500, // Provide your own width, height and frame rate here
                    // height: 300,
                    // minFrameRate: 30
                    //resolutions
                    //3840×2160 4k
                    //2048×1152 2k
                    //1920 × 1080 1080p
                    //1280 × 720 720p
                    //1024	×	576
                    maxWidth: 1920, // Provide your own width, height and frame rate here
                    maxHeight: 1080,
                    minWidth: 1280, // Provide your own width, height and frame rate here
                    minHeight: 720,
                    minFrameRate: 30
                },
                facingMode: (false ? "user" : "environment"),
                optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
            }
        }, function (stream) {
            console.log('dddd', stream);
            callback(stream);
        }, logError);
    });
}

function join(roomID) {
    socket.emit('join', roomID, function (socketIds) {
        console.log('join', socketIds);
        for (const i in socketIds) {
            const socketId = socketIds[i];
            createPC(socketId, true);
        }
    });
}

function createPC(socketId, isOffer) {
    const pc = new RTCPeerConnection(configuration);
    pcPeers[socketId] = pc;

    pc.onicecandidate = function (event) {
        console.log('onicecandidate', event.candidate);
        if (event.candidate) {
            socket.emit('exchange', {'to': socketId, 'candidate': event.candidate});
        }
    };

    function createOffer() {
        pc.createOffer(function (desc) {
            console.log('createOffer', desc);
            pc.setLocalDescription(desc, function () {
                console.log('setLocalDescription', pc.localDescription);
                socket.emit('exchange', {'to': socketId, 'sdp': pc.localDescription});
            }, logError);
        }, logError);
    }

    pc.onnegotiationneeded = function () {
        console.log('onnegotiationneeded');
        if (isOffer) {
            createOffer();
        }
    }

    pc.oniceconnectionstatechange = function (event) {
        console.log('oniceconnectionstatechange', event.target.iceConnectionState);
        if (event.target.iceConnectionState === 'completed') {
            setTimeout(() => {
                getStats();
            }, 1000);
        }
        if (event.target.iceConnectionState === 'connected') {
            createDataChannel();
        }
    };
    pc.onsignalingstatechange = function (event) {
        console.log('onsignalingstatechange', event.target.signalingState);
    };

    pc.onaddstream = function (event) {
        console.log('onaddstream', event.stream);
        container.setState({info: 'One peer join!'});

        const remoteList = container.state.remoteList;
        remoteList[socketId] = event.stream.toURL();
        container.setState({remoteList: remoteList});
    };
    pc.onremovestream = function (event) {
        console.log('onremovestream', event.stream);
    };

    pc.addStream(localStream);
    function createDataChannel() {
        if (pc.textDataChannel) {
            return;
        }
        const dataChannel = pc.createDataChannel("text");

        dataChannel.onerror = function (error) {
            console.log("dataChannel.onerror", error);
        };

        dataChannel.onmessage = function (event) {
            console.log("dataChannel.onmessage:", event.data);
            container.receiveTextData({user: socketId, message: event.data});
        };

        dataChannel.onopen = function () {
            console.log('dataChannel.onopen');
            container.setState({textRoomConnected: true});
        };

        dataChannel.onclose = function () {
            console.log("dataChannel.onclose");
        };

        pc.textDataChannel = dataChannel;
    }

    return pc;
}

function exchange(data) {
    const fromId = data.from;
    let pc;
    if (fromId in pcPeers) {
        pc = pcPeers[fromId];
    } else {
        pc = createPC(fromId, false);
    }

    if (data.sdp) {
        console.log('exchange sdp', data);
        pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
            if (pc.remoteDescription.type == "offer")
                pc.createAnswer(function (desc) {
                    console.log('createAnswer', desc);
                    pc.setLocalDescription(desc, function () {
                        console.log('setLocalDescription', pc.localDescription);
                        socket.emit('exchange', {'to': fromId, 'sdp': pc.localDescription});
                    }, logError);
                }, logError);
        }, logError);
    } else {
        console.log('exchange candidate', data);
        pc.addIceCandidate(new RTCIceCandidate(data.candidate));
    }
}

function leave(socketId) {
    console.log('leave', socketId);
    const pc = pcPeers[socketId];
    const viewIndex = pc.viewIndex;
    pc.close();
    delete pcPeers[socketId];

    const remoteList = container.state.remoteList;
    delete remoteList[socketId]
    container.setState({remoteList: remoteList});
    container.setState({info: 'One peer leave!'});
}

function logError(error) {
    console.log("logError", error);
}

function mapHash(hash, func) {
    const array = [];
    for (const key in hash) {
        const obj = hash[key];
        array.push(func(obj, key));
    }
    return array;
}

function getStats() {
    const pc = pcPeers[Object.keys(pcPeers)[0]];
    if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0]) {
        const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
        console.log('track', track);
        pc.getStats(track, function (report) {
            console.log('getStats report', report);
        }, logError);
    }
}

let container;

const RCTWebRTCDemo = React.createClass({
    getInitialState: function () {
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => true});
        return {
            info: 'Initializing',
            status: 'init',
            roomID: '',
            isFront: true,
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: ''
        };
    },
    componentDidMount: function () {
        container = this;
        // socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});
        socket = io.connect(this.props.webRTCServer, {transports: ['websocket']});
        socket.on('exchange', function (data) {
            exchange(data);
        });
        socket.on('leave', function (socketId) {
            leave(socketId);
        });

        socket.on('connect', function (data) {
            console.log('connect');
            getLocalStream(true, function (stream) {
                localStream = stream;
                container.setState({selfViewSrc: stream.toURL()});
                container.setState({status: 'ready', info: 'Please enter or create room ID'});
            });
        });
    },
    _press(event) {
        this.props.myData.UserTypeNo == 2 ?
            this.setState({roomID: JSON.stringify(this.props.myData.UserNo)})
            :
            this.setState({roomID: this.props.userNo})
        this.refs.roomID.blur();
        this.setState({status: 'connect', info: 'Connecting'});
        join(this.state.roomID);
    },
    _switchVideoType() {
        const isFront = !this.state.isFront;
        this.setState({isFront});
        getLocalStream(isFront, function (stream) {
            if (localStream) {
                for (const id in pcPeers) {
                    const pc = pcPeers[id];
                    pc && pc.removeStream(localStream);
                }
                localStream.release();
            }
            localStream = stream;
            container.setState({selfViewSrc: stream.toURL()});

            for (const id in pcPeers) {
                const pc = pcPeers[id];
                pc && pc.addStream(localStream);
            }
        });
    },
    receiveTextData(data) {
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push(data);
        this.setState({textRoomData, textRoomValue: ''});
    },
    _textRoomPress() {
        if (!this.state.textRoomValue) {
            return
        }
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push({user: 'Me', message: this.state.textRoomValue});
        for (const key in pcPeers) {
            const pc = pcPeers[key];
            pc.textDataChannel.send(this.state.textRoomValue);
        }
        this.setState({textRoomData, textRoomValue: ''});
    },
    _renderTextRoom() {
        return (
            <View style={styles.listViewContainer}>
                <ListView
                    dataSource={this.ds.cloneWithRows(this.state.textRoomData)}
                    renderRow={rowData => <Text>{`${rowData.user}: ${rowData.message}`}</Text>}
                />
                <TextInput
                    style={{width: 200, height: 30, borderColor: 'gray', borderWidth: 1}}
                    onChangeText={value => this.setState({textRoomValue: value})}
                    value={this.state.textRoomValue}
                />
                <TouchableHighlight
                    onPress={this._textRoomPress}>
                    <Text>Send</Text>
                </TouchableHighlight>
            </View>
        );
    },
    render() {
        return (
            <Container>
                <Content>
                    {/*{this.state.textRoomConnected && this._renderTextRoom()}*/}
                    {/*<View style={{flexDirection: 'row'}}>*/}
                    {/*/!*<Text>*!/*/}

                    {/*/!*</Text>*!/*/}
                    {/*</View>*/}
                    {this.props.myData.UserTypeNo == 2 ?
                        <View>
                            { this.state.status == 'ready' ?
                                (<View style={{justifyContent:'center'}}>
                                    <Text style={styles.welcome}>
                                        {this.state.info}
                                    </Text>
                                    <TextInput editable={false}
                                               ref='roomID'
                                               autoCorrect={false}
                                               style={{alignSelf:'center', width: 200, height: 40, borderColor: 'gray', borderWidth: 1, margin: 5}}
                                               onChangeText={(text) => this.setState({roomID: text})}
                                               value={JSON.stringify(this.props.myData.UserNo)}
                                    />

                                    <Button style={{alignSelf:'center', margin: 5}}
                                            onPress={this._press} success>
                                        <Text>Watch Now!!!</Text>
                                    </Button>
                                    {this.props.myData.UserTypeNo == 2 ? (
                                            <View>
                                                <Text>{this.state.isFront ? "Use front camera" : "Use back camera"}</Text>
                                                <Button style={{alignSelf:'center', margin: 5}}
                                                        onPress={this._switchVideoType}>
                                                    <Text>Fix camera!</Text>
                                                </Button>
                                            </View>) : null}
                                </View>) : null
                            }
                        </View>
                        :
                        <View>
                            { this.state.status == 'ready' ?
                                (<View style={{justifyContent:'center'}}>
                                    <Text style={styles.welcome}>
                                        {this.state.info}
                                    </Text>
                                    <TextInput editable={false}
                                               ref='roomID'
                                               autoCorrect={false}
                                               style={{alignSelf:'center', width: 200, height: 40, borderColor: 'gray', borderWidth: 1, margin: 5}}
                                               onChangeText={(text) => this.setState({roomID: text})}
                                               value={this.props.userNo}
                                    />

                                    <Button style={{alignSelf:'center', margin: 5}}
                                            onPress={this._press} success>
                                        <Text>Watch Now!!!</Text>
                                    </Button>
                                    {this.props.myData.UserTypeNo == 2 ?
                                        <Button style={{alignSelf:'center', margin: 5}}
                                                onPress={this._switchVideoType}>
                                            <Text>Fix camera!</Text>
                                        </Button> : null}
                                </View>) : null
                            }
                        </View>
                    }
                    {this.props.myData.UserTypeNo == 2 ?
                        <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView}/>
                        :
                        mapHash(this.state.remoteList, function (remote, index) {
                            return <RTCView key={index} streamURL={remote} style={styles.remoteView}/>
                        })
                    }
                </Content>
            </Container>
        );
    }
});

const styles = StyleSheet.create({
    selfView: {
        minWidth: width,
        minHeight: height, alignSelf: 'center'
    },
    remoteView: {
        minWidth: width,
        minHeight: height, alignSelf: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    listViewContainer: {
        height: 150,
    },
});

export default RCTWebRTCDemo;
// AppRegistry.registerComponent('VirtualTraverse', () => RCTWebRTCDemo);
