/**
 * Created by DelaCerna on 12/8/16.
 */
'use strict';
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Picker,
    Image,
    TextInput,
    TouchableHighlight,
    ScrollView,
    ListView,
} from "react-native"
import {Actions} from "react-native-router-flux";
import {Dimensions} from "react-native";
var {height, width} = Dimensions.get('window');


export default class extends React.Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(['1', '2', '3', '4', '5', '6', '7']),
        };
    }

    render() {
        return (
            <Container>
                <Content>
                    <Card>
                        {/*<Button rounded success onPress={()=>this.requestTour(Username)}*/}
                        {/*style={{width: 90, height: 90, borderRadius: 50, backgroundColor: '#ee09df', position:'absolute',zIndex:1, right:5, top:5}}>*/}
                        {/*/!*<Icon active name='ios-eye'/>*!/*/}
                        {/*<Text style={{fontSize:17, alignSelf:'center'}}>Hire</Text>*/}
                        {/*</Button>*/}
                        <CardItem>
                            <View style={{width:width*.75}}>
                                <Left>
                                    {/*<Thumbnail source={{uri:'https://unsplash.it/600/400/?random'}}/>*/}
                                    <Button onPress={()=>this.checkRanked('s', 's')}
                                            style={{justifyContent:'center',backgroundColor:'yellow',width: 60, height: 60, borderRadius: 30}}>
                                        <Text
                                            style={{alignSelf:'center', color: 'red', fontWeight: 'bold', fontSize: 25}}>{Rank}</Text>
                                    </Button>
                                    <Body>
                                    <Text>
                                        DestinationName
                                    </Text>
                                    <Text note>
                                       Username
                                    </Text>
                                    </Body>
                                </Left>
                            </View>
                        </CardItem>
                        <View style={{width:width*.75}}>
                            <CardItem content>

                                <Text>
                                    DestinationDesc
                                </Text>
                            </CardItem>
                        </View>
                        <CardItem>
                            <Left>
                                <Text>CategoryName</Text>

                                {/*<Icon active name="star" style={{color: '#ffc125', fontSize:20}}/>*/}
                                {/*<Icon active name="star" style={{color: '#ffc125', fontSize:20}}/>*/}
                                {/*<Icon active name="star" style={{color: '#ffc125', fontSize:20}}/>*/}
                                {/*<Icon active name="star" style={{color: '#ffc125', fontSize:20}}/>*/}
                                {/*<Icon active name="star" style={{color: 'black', fontSize:20}}/>*/}
                            </Left>


                            {/*<Icon active name="chatbubbles"/>*/}
                            {/*<Text>Comments</Text>*/}
                            <Text>Pricephp/hr</Text>


                            <Right>
                                <Button bordered>
                                    <Text>details</Text>
                                </Button>
                            </Right>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
