/**
 * Created by DelaCerna on 2/8/17.
 */
/**
 * Created by DelaCerna on 1/25/17.
 */
import {
    View,
    StyleSheet,
    Modal, Picker, Dimensions, Text, Button, ScrollView
} from "react-native"
import {Thumbnail, Item, InputGroup, Input, Icon, CheckBox} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
import {Container, Content} from 'native-base';
import VTListTourComments from './VTListTourComments';
var {height, width} = Dimensions.get('window');
export default class extends Component {

    render() {
        return (
            <View style={{flex:1,backgroundColor: 'gray',alignItems:'center', padding:5}}>

                <View style={{height:height*.45,backgroundColor: 'gray',alignItems:'center', padding:5}}>
                    <ScrollView style={{height: height * .3, width:width * .9}}
                                horizontal={false}>
                        <Text style={{fontSize:20}}>Rated: 5/10</Text>
                        <View style={{width:width*.9, height:height*.5, backgroundColor:'skyblue', padding:5}}>
                            <Text style={{fontSize:30}}>Bohol Escapade</Text>
                            <Text style={{fontWeight: 'bold'}}>Description</Text>
                            <Text>
                                There are thousands of reasons for you to visit this island-province paradise. Reason #1 to #1,776 – Bohol's limestone Chocolate Hills. Reason #1,777 – the white-sand beaches of Panglao Island. Reason #1,778 and beyond – the lush underwater world of the Balicasag islet, the languid and scenic Loboc River, the organic Bohol Bee Farm, the exciting Chocolate Hills Adventure Park, the adorable tarsier… there isn’t enough space here to list them all!
                            </Text>
                            <Text style={{fontWeight: 'bold'}}>Tourist guide: Jose Del Rosario</Text>
                            <Button title="view guide" onPress={()=>[Actions.gdetail, console.log('buttonclicked sss')]}/>
                        </View>
                    </ScrollView>
                </View>
                <VTListTourComments/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputGroup: {
        backgroundColor: 'gray',
        margin: 5
    },
    mainContainer: {
        height: height,
        alignItems: 'center',
        // backgroundColor: 'black',
        padding: 10, justifyContent: 'center'
    }
});
