import React, {Component} from 'react';
import {AppRegistry, View, Button, StyleSheet, TextInput, Dimensions, Text, ToastAndroid, Picker} from 'react-native';
import {Container, Content, Thumbnail} from 'native-base';
const {width, height} = Dimensions.get('window');
let tourCategories;
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {myData:''};
    }
    componentWillMount(){
        this.setState({
            // username:this.props.myData.Username,
            // password:this.props.myData.Password,
            // email:this.props.myData.Email,
            // name:this.props.myData.FirstName + ' '+ this.props.myData.MiddleName+ ' '+this.props.myData.LastName,
            // contact:this.props.myData.ContactNo,
            // address:this.props.myData.Address,
        })
        this.getCategories()
    }

    render() {
        return (
            <Container style={{backgroundColor:'black'}}>
                <Content>
                    <View style={[styles.centerAll,{alignItems:'stretch', backgroundColor:'white'}]}>
                        <TextInput editable={this.state.editable}
                                   style={styles.textInput}
                                   onChangeText={(username) => this.setState({username})}
                                   value={this.state.username}
                                   placeholder={"Tour Name"}

                        />
                        <TextInput editable={this.state.editable}
                                   style={[styles.textInput, {height:height*.3}]}
                                   onChangeText={(password) => this.setState({password})}
                                   value={this.state.password}
                                   placeholder={"Tour Description"}
                                   maxLength = {180}
                                   numberOfLines = {5}
                                   multiline = {true}
                        />
                        <Text>Category</Text>
                        <Picker
                            selectedValue={this.state.location}
                            onValueChange={(lang) => this.setState({location: lang})}>

                            <Picker.Item label={''} value={'a'} />
                            <Picker.Item label={'Bohol'} value={'b'} />
                            <Picker.Item label={'Cebu'} value={'c'} />
                            <Picker.Item label={'Malapascua'} value={'d'} />
                            <Picker.Item label={'Palawan'} value={'e'} />
                        </Picker>
                        {/*{*/}
                        {/*this.props.sessiontype == 1 ? <TextInput editable = {this.state.disable}*/}
                        {/*style={{height: 40, borderColor: 'gray', borderWidth: 1}}*/}
                        {/*onChangeText={(username) => this.setState({username}, console.log(this.state.username))}*/}
                        {/*value={this.state.username}*/}
                        {/*placeholder={'EXAMPLE VIDEO URL'}*/}
                        {/*/> : null*/}
                        {/*}*/}


                        <View style={styles.centerAll}>
                            <View style={styles.button}>
                                <Button onPress={()=>this.logout()}
                                        title="Done"
                                        accessibilityLabel="Learn more about this purple button"
                                />
                            </View>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }

    editDetails() {
        this.setState({editable: true})
    }

    saveDetails() {
        this.setState({editable: false})

    }

    getCategories() {
        console.log('clicked logout')
        fetch(+'/get/category', {
            method: 'get',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                this.setState({myData: responseData})
                console.log('myData: 1'+JSON.stringify(responseData))
                // tourCategories = responseData;
                // return responseData;
            })
        ToastAndroid.show('You\'re logged out', ToastAndroid.SHORT);
    }
};

const styles = StyleSheet.create({
    inputGroup: {
        backgroundColor: 'gray',
        margin: 5
    },
    mainContainer: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: 'black',
        padding: 10
    },
    centerAll: {
        alignItems: 'center', padding: 5, margin: 5
    },
    button: {
        padding: 5, margin: 5, width: width * .5
    },
    textInput: {
        height: 40, borderColor: 'gray', borderWidth: 1, padding: 5, margin: 5
    }
});