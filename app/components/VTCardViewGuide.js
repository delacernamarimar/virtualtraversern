/**
 * Created by DelaCerna on 1/25/17.
 */
'use strict';
import React, {Component} from 'react';
import {Image, View, Dimensions, ToastAndroid, Alert, ScrollView} from 'react-native';
import {Button, Card, CardItem, Left, Body, Text, ListItem, Right, List, Icon, Item, Label} from 'native-base';
const {width, height} = Dimensions.get('window');
import CacheStore from 'react-native-cache-store';
import {Actions} from "react-native-router-flux";
let Price, Rank, mqttServer, Username, DestinationName, DestinationDesc, CategoryName, Status, OfferNo, UserNo, databaseServer;
export default class extends Component {
    constructor() {
        super()
        this.state = {
            testing: '', sessionUserData: '', comments: '', tourDeleted: false, showComments: false,
        }
    }

    render() {
        databaseServer = this.props.databaseServer
        mqttServer = this.props.mqttServer;
        Username = this.props.sessionUserData.Username;
        DestinationName = this.props.tours[this.props.counter].DestinationName;
        DestinationDesc = this.props.tours[this.props.counter].DestinationDesc;
        CategoryName = this.props.tours[this.props.counter].CategoryName;
        Status = this.props.tours[this.props.counter].Status;
        Rank = this.props.tours[this.props.counter].Rank;
        Price = this.props.tours[this.props.counter].Price;
        OfferNo = this.props.tours[this.props.counter].OfferNo;
        UserNo = this.props.sessionUserData.UserNo;
        return (
            this.state.tourDeleted ? null :
                <ListItem>
                    <Card>
                        <CardItem>
                            <View style={{width:width*.75}}>
                                <Left>
                                    {/*<Thumbnail source={{uri:'https://unsplash.it/600/400/?random'}}/>*/}
                                    <Button onPress={()=>this.checkRanked( Rank = this.props.tours[this.props.counter].Rank, Rank = this.props.tours[this.props.counter].RankDesc)}
                                            style={{justifyContent:'center',backgroundColor:'yellow',width: 60, height: 60, borderRadius: 30}}>
                                        <Text
                                            style={{alignSelf:'center', color: 'red', fontWeight: 'bold', fontSize: 25}}>{Rank}</Text>
                                    </Button>
                                    <Body>
                                    <Text>
                                        {DestinationName}
                                    </Text>
                                    <Text note>
                                        {Username}
                                    </Text>
                                    </Body>
                                    <Right>
                                        <Button bordered onPress={()=>this.removeTour()}>
                                            <Text>Remove</Text>
                                        </Button>
                                    </Right>
                                </Left>

                            </View>
                        </CardItem>
                        <CardItem cardBody>
                            <Image/>
                        </CardItem>
                        <View style={{width:width*.75}}>
                            <CardItem content>

                                <Text>
                                    {DestinationDesc}
                                </Text>
                            </CardItem>
                        </View>
                        <CardItem>
                            <Left>
                                <Text>{CategoryName}</Text>
                            </Left>
                            <Text>{Price}php/hr</Text>
                            <Right>
                                <Button bordered onPress={()=>this.showDetails()}>
                                    <Text>Details</Text>
                                </Button>
                            </Right>
                        </CardItem>
                        {this.state.showComments ?


                            <List dataArray={this.state.comments} renderRow={(data) =>
                            <ScrollView horizontal={true}>
                        <ListItem>
                            <CardItem>
                                    <View>
                                    <Item stackedLabel>
                                        <Label>Tourist</Label>
                                    <Text>{data.Username}</Text>
                                    </Item>
                                    <Item stackedLabel>
                                        <Label>Rate</Label>
                                    {data.Rate == 1 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 2 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 3 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 4 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 5 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    </Item>
                                    <Item stackedLabel>
                                        <Label>Comment</Label>
                                    <Text>{data.Review}</Text>
                                    </Item>
                    </View>
                    </CardItem>
                        </ListItem>
                        </ScrollView>
                    }/>

                            : null}

                    </Card>
                </ListItem >

        );
    }

    showDetails() {
        this.setState({showComments: !this.state.showComments})
        if (this.state.showComments) {
        } else {
            fetch(this.props.databaseServer + '/get/tours/guide/transaction/tour/' + this.props.sessionUserData.UserNo + '/' + this.props.tours[this.props.counter].OfferNo, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    return response.json() // << This is the problem
                })
                .then((responseData) => { // responseData = undefined
                    if (responseData.length == 0) {
                        ToastAndroid.show('No rates and comments yet.', ToastAndroid.SHORT);
                    } else {
                        this.setState({comments: responseData})
                    }
                }).catch(function (error) {
                Alert.alert(
                    'Error!',
                    'Can\'t connect to to server! \n Make sure you have internet connections!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
        }
    }

    removeTour() {
        Alert.alert(
            'Confirm!',
            'Are you sure you want to remove this tour?',
            [
                {
                    text: 'CANCEL!'

                }, {
                text: 'OK!', onPress: () => this.removeTourNow()

            },
            ],
            {cancelable: false}
        )


    }

    removeTourNow() {
        fetch(this.props.databaseServer + '/get/tours/guide/tour/delete/' + this.props.sessionUserData.UserNo + '/' + this.props.tours[this.props.counter].OfferNo, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                ToastAndroid.show('Tour deleted!', ToastAndroid.SHORT)
                this.setState({tourDeleted: true})
                CacheStore.set('tours', responseData);
            })
            .catch(function (error) {
                Alert.alert(
                    'asf!',
                    'Can\'t connect to to server! \n Make sure you have internet connections!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
    }

    checkRanked(rank, rankdesc) {
        Alert.alert(
            rank,
            rankdesc,
            [
                {
                    text: 'OK!', onPress: () => console.log("OK")
                },
            ],
            {cancelable: false}
        )
    }

    // requestTour(username){
    //     var mqtt    = require('react-native-mqtt');
    //     /* create mqtt client */
    //     mqtt.createClient({
    //         // uri: 'mqtt://test.mosquitto.org:1883',
    //         uri: mqttServer,
    //
    //         clientId: 'tourist'
    //     }).then(function(client) {
    //
    //         client.on('closed', function() {
    //             console.log('mqtt.event.closed');
    //
    //         });
    //
    //         client.on('error', function(msg) {
    //             console.log('mqtt.event.error', msg);
    //
    //         });
    //
    //         client.on('message', function(msg) {
    //             console.log('mqtt.event.message', msg);
    //             if(msg == 'accepted'){
    //                 // Actions.RCTWebRTCDemo({sessionUserData:sessionUserData , userNo:userNo})
    //             }else{
    //
    //             }
    //         });
    //
    //         client.on('connect', function() {
    //             console.log('connected');
    //             client.subscribe('/data/'+username , 0);
    //             client.publish('/data/'+username, 'request', 0, false);
    //         });
    //         client.connect();
    //     }).catch(function(err){
    //         console.log(err);
    //     });
    // }
}