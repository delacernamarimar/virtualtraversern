import React, {Component} from 'react';
import {AppRegistry, View, StyleSheet, TextInput, Dimensions, Text, ToastAndroid, Alert} from 'react-native';
import {
    Container,
    Content,
    Button,
    Form,
    Input,
    Label,
    Item,
    ListItem,
    Radio,
    Left,
    Header,
    Right,
    Body,
    Icon,
    Title
} from 'native-base';
import {Actions} from 'react-native-router-flux';
const {width, height} = Dimensions.get('window');
let Username;
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DOB: '', Username: '', Password: '', Email: '', FirstName: '', MiddleName: '', Gender: '',
            LastName: '', ContactNo: '', Address: '', editable: false, genderMale: true, genderFemale: false
            , userActive: false, userInactive: false
        };
    }

    componentDidMount() {
        this.setState({
            Gender: this.props.sessionUserData.Gender == 'M' ? this.setState({
                    genderMale: true,
                    genderFemale: false
                }) : this.setState({genderMale: false, genderFemale: true}),
            DOB: this.props.sessionUserData.Gender != null ? this.props.sessionUserData.DOB : '1990-30-12',
            Username: this.props.sessionUserData.Username,
            Password: this.props.sessionUserData.Password,
            Email: this.props.sessionUserData.Email,
            FirstName: this.props.sessionUserData.FirstName,
            MiddleName: this.props.sessionUserData.MiddleName,
            LastName: this.props.sessionUserData.LastName,
            ContactNo: this.props.sessionUserData.ContactNo,
            Address: this.props.sessionUserData.Address,
        })
        Username = this.props.sessionUserData.Username;
    }

    render() {
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content>
                    <Header style={{backgroundColor:'#ee09df'}}>
                        <Left>
                        </Left>
                        <Body>
                        <Title>Payment</Title>
                        </Body>
                        <Right>
                        </Right>
                    </Header>
                        <Form>
                            <Item stackedLabel>
                                <Label>Guide Username</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Destination Name</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Destination Category</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Paypal ID</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Amount</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Durations</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Rate</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Item stackedLabel>
                                <Label>Ban</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>

                            <Item stackedLabel>
                                <Label>Comment</Label>
                                <Input style={{width:width*.8}}/>
                            </Item>
                            <Button bordered info style={{margin:5}} block >
                                <Text>Pay Now!</Text>
                            </Button>
                        </Form>
                </Content>
            </Container>
        );
    }
};
