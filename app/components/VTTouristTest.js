import React, {Component} from 'react';
import {AppRegistry, View, Button, ToastAndroid, Text} from 'react-native';
import {Container, Content,} from 'native-base';
import {Actions} from "react-native-router-flux";
import RCTWebRTCDemo from './RCTWebRTCDemo'
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false, videoCall:false
        };
    }
    render() {
        return (
            <Container style = {{padding: 5}}>
                <Content>
                    <Text>{JSON.stringify(this.props.myData)}</Text>
                    <Button onPress={()=>this.videoCall()}
                            title="Try Video Call"
                            color="#841584"
                            accessibilityLabel="Learn more about this purple button"
                    />
                    {this.state.videoCall ? <RCTWebRTCDemo myData={this.props.myData}/> : null}
                </Content>
            </Container>
        );
    }

    logout() {
        console.log('clicked logout')
        fetch('http://192.168.1.5/virtualtraverse/public/stopsession', {
            method: 'get',
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        ToastAndroid.show('You\'re logged out', ToastAndroid.SHORT);
    }

    videoCall() {
        this.setState({videoCall: true})
    }
};