/**
 * Created by DelaCerna on 1/11/17.
 */
import React from "react";
import {Dimensions} from "react-native";
var {height, width} = Dimensions.get('window');
module.exports = {

    getScreenHeight(){
        if(height > 1080){
            return height * .55}
        if (height > 960){
            return height * .65}
        //tab 7inches
        if (height > 910){
            return height * .65}
        //6inches
        if (height > 680){
            return height * .63}
        //5inches
        if (height > 570){
            return height * .85}
        //4inches
        if (height > 520){
            return height * .9}
        if (height > 470){
            return height * .8}
        if (height > 426){
            return height * .85}
        return height
    },
    getScreenWidth(){
        if (width > 720){
            return width * .65}
        //tab 7inches
        if (width > 480){
            return width * .6}
        //6inches
        if (width > 410){
            return width * .65}
        //5inches
        if (width > 320){
            return width * .85}
        //4inches
        if (width > 270){
            return width * .9}
        return width
    },
    getWidth(){
        return width
    },
    getHeight(){
        return height
    }
}
