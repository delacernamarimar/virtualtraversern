import React, {Component} from 'react';
import {AppRegistry, View, Button} from 'react-native';
import {Actions} from "react-native-router-flux";
import VTHomePageGuide from './VTHomePageGuide';
import VTHomePageTourist from './VTHomePageTourist';
import VTVideoGuide from './VTVideoGuide';
import VTVideoTourist from './VTVideoTourist';
export default class extends Component {
    render() {
        return (
            this.props.sessionUserData.UserTypeNo == 1 ?
                <VTHomePageTourist
                    sessionUserData={this.props.sessionUserData} databaseServer={this.props.databaseServer}
                    mqttServer={this.props.mqttServer}
                /> :
                <VTHomePageGuide
                    sessionUserData={this.props.sessionUserData} databaseServer={this.props.databaseServer}
                    mqttServer={this.props.mqttServer}
                />
            // this.props.sessionUserData.UserTypeNo == 1 ?
            //     <VTVideoTourist
            //         myData={this.props.myData} databaseServer={this.props.databaseServer}
            //         mqttServer={this.props.mqttServer}
            //     /> :
            //     <VTVideoGuide
            //         myData={this.props.myData} databaseServer={this.props.databaseServer}
            //         mqttServer={this.props.mqttServer}
            //     />
        );
    }
};