/**
 * Created by DelaCerna on 2/8/17.
 */
/**
 * Created by DelaCerna on 12/6/16.
 */
'use strict';
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Picker,
    Image,
    TextInput,
    TouchableHighlight,
    ScrollView,
    Switch, Button
} from "react-native"
import {Actions} from "react-native-router-flux";
import {Dimensions} from "react-native";
import CheckBox from 'react-native-checkbox';
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF",
    },
});

const popToRoot = () => {
    Actions.popTo("root");
}

const popToLogin1 = () => {
    Actions.popTo("loginModal");
}

const popToLogin2 = () => {
    Actions.popTo("loginModal2");
}

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: 'guide'};
    }
    render(){
        return (
//             <View style={styles.container}>
//                 <Text>Login2 page: {this.props.data}</Text>
//                 <Button onPress={Actions.pop}>Back</Button>
//                 <Button onPress={popToLogin1}>To Login</Button>
//                 <Button onPress={popToLogin2}>To Login2</Button>
//                 <Button onPress={popToRoot}>To Root</Button>
//             </View>

            <View
                style={{
                    flex:1,margin: width *.05
                }}>
                <View
                    style={{
                     flexDirection:'row',
                     justifyContent: 'flex-start',
                     alignItems: 'stretch'
                   }}>
                    <View>
                        <TextInput
                            style={{padding: 5,
                       height:  height * .05 ,
                       width:  width * .5
                     }}
                            placeholder={'Filter Search'}
                            placeholderTextColor={"rgba(198,198,204,1)"}
                            onChangeText={(text) => {this.setState({text})}}
                            onSubmitEditing={() => {this.setState({text: ''})}}
                            value={(this.state && this.state.text) || ''}
                        />
                    </View>
                    <View>
                        <Image
                            style={{ padding: 5,
                       width:  width * .2 ,
                       height:  height * .05 ,
                     }}
                            resizeMode={"contain"}
                            source={{uri:'https://unsplash.it/600/400/?random'}}
                        /></View>
                </View>
                <Text
                    style={{
                color: 'black',
                fontSize: 16,
                fontWeight: 'normal',
                fontFamily: 'Helvetica Neue',
              }}>
                    Location
                </Text>
                <Picker
                    selectedValue={this.state.location}
                    onValueChange={(lang) => this.setState({location: lang})}>
                    <Picker.Item label={'All'} value={'a'} />
                    <Picker.Item label={'Bohol'} value={'b'} />
                    <Picker.Item label={'Cebu'} value={'c'} />
                    <Picker.Item label={'Malapascua'} value={'d'} />
                    <Picker.Item label={'Palawan'} value={'e'} />
                </Picker>
                <Text
                    style={{
                color: 'black',
                fontSize: 16,
                fontWeight: 'normal',
                fontFamily: 'Helvetica Neue',
              }}>
                    Guide
                </Text>
                <Picker
                    selectedValue={this.state.ranked}
                    onValueChange={(lang) => this.setState({ranked: lang})}>
                    <Picker.Item label={'Ranked-S'} value={'a'} />
                    <Picker.Item label={'Ranked-A'} value={'b'} />
                    <Picker.Item label={'Ranked-B'} value={'c'} />
                    <Picker.Item label={'Ranked-C'} value={'d'} />
                    <Picker.Item label={'Ranked-D'} value={'e'} />
                    <Picker.Item label={'Ranked-E'} value={'f'} />
                </Picker>
                <ScrollView
                    horizontal={false}>
                    <Text
                        style={{
                  color: 'black',
                  fontSize: 16,
                  fontWeight: 'normal',
                  fontFamily: 'Helvetica Neue',
                }}>
                        CATEGORIES
                    </Text>
                    <CheckBox
                        label='Airport'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Beach'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Churches'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Dungeons'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Enterprises'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Fields'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Greenhouses'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Airport'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Beach'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Churches'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Dungeons'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Enterprises'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Fields'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Greenhouses'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Airport'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Beach'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Churches'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Dungeons'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Enterprises'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Fields'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                    <CheckBox
                        label='Greenhouses'
                        checked={false}
                        onChange={(checked) => console.log('I am checked', checked)}
                    />
                </ScrollView>
                  <Button  title="Search" onPress={()=>console.log('search')}/>
            </View>
        );
    }
}
