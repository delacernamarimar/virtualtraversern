/**
 * Created by DelaCerna on 2/8/17.
 */
'use strict';

import CardView from './CardView';
import {
    View,
    StyleSheet,
    Modal, Picker, Image,
    TextInput,
    ScrollView,
    ListView, Dimensions, Button, Text
} from "react-native"
import {ButtonItem} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
var {height, width} = Dimensions.get('window');
export default class extends Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows([
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!',
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!',
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!',
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!',
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!',
                '9:30pm\t 30mins\t Rice Terraces excapade!',
                '12:30pm\t 1hr\t Eating at Chocolate Hill!'
            ]),
        };
    }
    render(){
        return (
            <View style={{alignItems:'center' , backgroundColor:'gray'}}>
                <ScrollView style={{height: height * .5, width:width * .9}}
                            horizontal={false}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) =>
                        <View style={{margin:5}}>
                        <Text style = {{fontWeight:'bold', textAlign:'center'}}> {rowData}</Text>
                        <Button onPress={Actions.VTTouristTourHistory} title={'view details'} rounded style={{margin:5, width:width*.4}}></Button>
                        </View>
                        }
                    />
                </ScrollView>
            </View>

        );
    }
}
