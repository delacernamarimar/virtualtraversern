import {
    View,
    StyleSheet,
    Modal, Picker, Dimensions, TextInput, Alert, ToastAndroid
} from "react-native"
import {Item, Text, CheckBox, Button, ListItem, Input, Body, Form, Label} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
import {Container, Content} from 'native-base';
var {height, width} = Dimensions.get('window');
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userType: 'Tourist',
            selectedItem: undefined,
            selected1: 'key0', username:'', password:'', email:'', password2:'',
            results: {
                items: [],
            }, userAgreement: false
        };
    }

    //noinspection JSAnnotator
    onValueChange(value: string) {
        this.setState({
            selected1: value, userType: 'Tourist', type:'Tourist'
        });
    }

    render() {
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content>
                        <Form>
                            <Item floatingLabel>
                                <Label>Username</Label>
                                <Input style={{width:width*.8}}
                                       onChangeText={(username) => this.setState({username:username})}
                                       value={this.state.username}/>
                            </Item>
                            <Item floatingLabel last>
                                <Label>Password</Label>
                                <Input style={{width:width*.8}} secureTextEntry = {true}
                                       onChangeText={(password) => this.setState({password:password})}
                                       value={this.state.password}/>
                            </Item>
                            <Item floatingLabel last>
                                <Label>Confirm Password</Label>
                                <Input style={{width:width*.8}} secureTextEntry = {true}
                                       onChangeText={(password2) => this.setState({password2:password2})}
                                       value={this.state.password2}/>
                            </Item>
                            <Item floatingLabel last>
                                <Label>Email</Label>
                                <Input style={{width:width*.8}}
                                       onChangeText={(email) => this.setState({email:email})}
                                       value={this.state.email}/>
                            </Item>
                        </Form>

                    <ListItem>
                        <CheckBox checked={this.state.userAgreement} onPress={()=>this.userAgreement()} />
                        <Body>
                        <Text>User Agreement</Text>
                        </Body>
                    </ListItem>

                            <Button bordered  style={{margin:5}} block  onPress={()=>this.registerUser(this.state.username,this.state.password,this.state.password2,this.state.email)}>
                                <Text>SIGN-UP</Text>
                            </Button>

                        <Text style={{color:'black', textAlign:'center', margin:5}}>or</Text>

                            <Button bordered  style={{ margin:5}} block><Text>facebook</Text></Button>


                            <Button bordered  style={{ margin:5}} block ><Text>google+</Text></Button>

                </Content>
            </Container>
        );
    }

    registerUser(username, password, password2, email) {
        if(password == password2) {
            fetch(this.props.databaseServer+'/get/user/add', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: username,
                    email: email,
                    password: password

                })
            }).then((response) => {
                return response.json() // << This is the problem
            }).then((responseData) => {// responseData = undefined
                    if(responseData.result == 'email existed')
                        ToastAndroid.show('Email existed!', ToastAndroid.SHORT);
                    if(responseData.result == 'user existed')
                        ToastAndroid.show('User existed!', ToastAndroid.SHORT);
                    if(responseData.result == 'created') {
                        ToastAndroid.show('User created!', ToastAndroid.SHORT);
                        Actions.pop()
                    }
                }).catch(function (error) {
                Alert.alert(
                    'Error!',
                    'Input all fields and secure internet connection!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log('OK')
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
        }else{
            Alert.alert(
                'Error!',
                'Password doesn\'t matched!',
                [
                    {
                        text: 'OK!', onPress: () => console.log('OK')
                    },
                ],
                {cancelable: false}
            )
        }

    }

    userAgreement() {
        if (this.state.userAgreement == true) {
            this.setState({userAgreement: false});
        }
        else {
            this.setState({userAgreement: true});
        }
    }
}

const styles = StyleSheet.create({
    inputGroup: {
        alignSelf: 'center', width: width * .4,
        backgroundColor: 'white',
        margin: 5
    },
    mainContainer: {
        alignItems: 'stretch',
        backgroundColor: 'white',
        padding: 5
    }
});
