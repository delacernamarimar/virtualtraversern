import React, {Component} from 'react';
import {Image} from 'react-native';
import {Content, ListItem, List, Text} from 'native-base';
import VTTransactionList from './VTTransactionList'


export default class extends Component {
    render() {

        var counter = -1;
        var date = this.props.data[counter+1].Date;
        return (
            <Content>
                <ListItem itemDivider>
                    <Text>{date.substring(0, 10)}</Text>
                </ListItem>
                <List horizontal dataArray={this.props.data} renderRow={(data) =>
                    <ListItem>
                        <VTTransactionList
                        sessionUserData={this.props.sessionUserData}
                        mqttServer={this.props.mqttServer}
                        tours = {this.props.tours}
                        data = {data}
                        counter = {counter=counter+1}/>
                    </ListItem>
                    }/>
            </Content>
        );
    }
}