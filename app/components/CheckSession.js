import React, {Component} from 'react';
import {AppRegistry, View, Button, AppState, Alert, ToastAndroid, AsyncStorage} from 'react-native';
import {Actions} from "react-native-router-flux";
import {Spinner, Container, Content} from 'native-base';
import CacheStore from 'react-native-cache-store';
export default class extends Component {
    constructor() {
        super()
        this.state = {
            session: '', sessionUser: false
        }

    }

    componentDidMount() {
        fetch(this.props.databaseServer + '/showsession', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                return response.json() // << This is the problem
            })
            .then((responseData) => { // responseData = undefined
                if (responseData != null) {
                    this.setState({session: responseData})
                    CacheStore.set('session', responseData);
                    Actions.NavigationDrawer({sessionUserData: responseData})
                } else {
                    Actions.VTLoginPage()
                }
            }).catch(function (error) {
            // Error retrieving data
            CacheStore.get('session').then((value) => {
                 // ToastAndroid.show('mySession:' + JSON.stringify(value), ToastAndroid.SHORT)
                if (value != null) {
                    Actions.NavigationDrawer({sessionUserData: value})
                } else {
                    Actions.VTLoginPage()
                }
            });

            // Alert.alert(
            //     'Error!',
            //     'Can\'t connect to to server! \n Make sure you have internet connections!',
            //     [
            //         {
            //             text: 'OK!', onPress: () => console.log("OK")
            //         },
            //     ],
            //     {cancelable: false}
            // )
            throw error;
        });
    }

    render() {
        return (
            <Container>
             <View style = {{justifyContent:'center', flex:1}}>
                    <Spinner/>
             </View>
            </Container>
        );
    }
};