/**
 * Created by DelaCerna on 1/25/17.
 */
'use strict';
import React, {Component} from 'react';
import {Image, View, Dimensions, Alert, ToastAndroid, ScrollView} from 'react-native';
import {Button, Text, Card, CardItem, Left, Body, List, Label, Icon, Right, ListItem, Item} from 'native-base';
const {width, height} = Dimensions.get('window');
import {Actions} from "react-native-router-flux";
let TourNo, RankDesc, mqttServer, Username, DestinationName, DestinationDesc, CategoryName, Rank, Price, UserNo, cancelRequest;
export default class extends Component {
    constructor() {
        super()
        this.state = {
            testing: '', comments: '', showComments: false
        }
    }

    render() {
        mqttServer = this.props.mqttServer
        UserNo = this.props.tours[this.props.counter].UserNo;
        Username = this.props.tours[this.props.counter].Username;
        DestinationName = this.props.tours[this.props.counter].DestinationName;
        DestinationDesc = this.props.tours[this.props.counter].DestinationDesc;
        CategoryName = this.props.tours[this.props.counter].CategoryName;
        Rank = this.props.tours[this.props.counter].Rank;
        Price = this.props.tours[this.props.counter].Price;
        RankDesc = this.props.tours[this.props.counter].RankDesc;
        TourNo = this.props.tours[this.props.counter].OfferNo;
        return (
            <Card>
                <CardItem>
                    <View style={{width:width*.75}}>
                        <Left>
                            <Button onPress={()=>this.checkRanked(this.props.tours[this.props.counter].Rank, this.props.tours[this.props.counter].RankDesc)}
                                    style={{justifyContent:'center',backgroundColor:'yellow',width: 60, height: 60, borderRadius: 30}}>
                                <Text
                                    style={{alignSelf:'center', color: 'red', fontWeight: 'bold', fontSize: 25}}>{Rank}</Text>
                            </Button>
                            <Body>
                            <Text>
                                {DestinationName}
                            </Text>
                            <Text note>
                                {Username}
                            </Text>
                            </Body>
                            <Right>
                                <Button bordered onPress={()=>this.requestTour(this.props.tours[this.props.counter].Username,this.props.sessionUserData)}>
                                    <Text>HIRE</Text>
                                </Button>
                            </Right>
                        </Left>
                    </View>
                </CardItem>
                <View style={{width:width*.75}}>
                    <CardItem content>

                        <Text>
                            {DestinationDesc}
                        </Text>
                    </CardItem>
                </View>
                <CardItem>
                    <Left>
                        <Text>{CategoryName}</Text>
                    </Left>
                    <Text>{Price}php/hr</Text>
                    <Right>
                        <Button bordered onPress={()=>this.showDetails()}>
                            <Text>details</Text>
                        </Button>
                    </Right>
                </CardItem>
                {this.state.showComments ?
                    <List dataArray={this.state.comments} renderRow={(data) =>
                            <ScrollView horizontal={true}>
                        <ListItem>
                            <CardItem>
                                    <View>
                                    <Item stackedLabel>
                                        <Label>Tourist</Label>
                                    <Text>{data.Username}</Text>
                                    </Item>
                                    <Item stackedLabel>
                                        <Label>Rate</Label>
                                    {data.Rate == 1 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 2 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 3 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 4 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    {data.Rate == 5 ? <View style = {{flexDirection:'row'}}><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/><Icon name='ios-star'/></View> : null}
                                    </Item>
                                    <Item stackedLabel>
                                        <Label>Comment</Label>
                                    <Text>{data.Review}</Text>
                                    </Item>
                    </View>
                    </CardItem>
                        </ListItem>
                        </ScrollView>
                    }/>

                    : null}
            </Card>
        );
    }

    showDetails() {
        this.setState({showComments: !this.state.showComments})
        if (this.state.showComments) {
        } else {
            fetch(this.props.databaseServer + '/get/tours/guide/transaction/tour/' + this.props.tours[this.props.counter].UserNo + '/' + this.props.tours[this.props.counter].OfferNo, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    return response.json() // << This is the problem
                })
                .then((responseData) => { // responseData = undefined
                    if (responseData.length == 0) {
                        ToastAndroid.show('No rates and comments yet.', ToastAndroid.SHORT);
                    } else {
                        this.setState({comments: responseData})
                    }

                }).catch(function (error) {
                Alert.alert(
                    'Errors!',
                    'Can\'t connect to to server! \n Make sure you have internet connections!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
        }
    }

    getTourDetail(TourNo) {
        fetch(this.props.databaseServer + '/get/tours/tourist', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        }).then((responseData) => { // responseData = undefined
            CacheStore.set('tours', responseData);
            this.setState({tours: responseData, loading: false})
        })
            .catch(function (error) {
                throw error;
            });
    }

    checkRanked(rank, rankdesc) {
        Alert.alert(
            rank,
            rankdesc,
            [
                {
                    text: 'OK!', onPress: () => console.log("OK")
                },
            ],
            {cancelable: false}
        )
    }
    requestTour(username, sessionUserData) {
        try {
            var mqtt = require('react-native-mqtt');
            /* create mqtt client */
            mqtt.createClient({
                 uri: 'mqtt://test.mosquitto.org:1883',
                 // uri: 'mqtt://192.168.1.6:1883',
                clientId: 'tourist'
            }).then(function (client) {
                client.on('closed', function () {
                    console.log('mqtt.event.closed');

                });

                client.on('error', function (msg) {
                    console.log('mqtt.event.error', msg);

                });

                client.on('message', function (msg) {
                    console.log('mqtt.event.message', msg);

                    if(msg.data == 'denied'){
                        ToastAndroid.show('Request has been denied', ToastAndroid.SHORT);
                    }
                    if(msg.data == 'granted'){
                        client.publish('/data/'+username, 'startNow', 0, false)
                        client.disconnect()
                        Actions.VTVideoTourist({room:username, sessionUserData:sessionUserData})
                    }
                });

                client.on('connect', function () {
                    console.log('connected');
                    client.subscribe('/data/'+username, 0);
                    client.publish('/data/'+username, 'request', 0, false);
                        Alert.alert(
                            'Requesting!',
                            'Sending request.',
                            [
                                {text: 'STOP!', onPress: () => client.publish('/data/'+username, 'cancel', 0, false)}
                            ],
                            {cancelable: false}
                        )
                    // client.publish('/data', userNo, 0, false);
                });
                client.connect();
            }).catch(function (err) {
                console.log(err);
            });
        } catch (error) {
            console.error(error);
        }
    }
}