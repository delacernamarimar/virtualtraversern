/**
 * Created by DelaCerna on 2/8/17.
 */
'use strict';

import VTComments from './VTComments';
import {
    View,
    StyleSheet,
    Modal, Picker, Image,
    TextInput,
    ScrollView,
    ListView, Dimensions, Button, Text
} from "react-native"
import {ButtonItem} from 'native-base';
import {Actions} from "react-native-router-flux";
import React, {Component} from 'react';
var {height, width} = Dimensions.get('window');
export default class extends Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows([
             '1', '2','3','4','5'
            ]),
        };
    }
    render(){
        return (
            <View style={{alignItems:'center' , backgroundColor:'gray',height:height*.3}}>
                <Text style = {{fontSize:20}}>Comments and Rate</Text>
                    <ScrollView style={{height: height * .3, width:width * .9}}
                                horizontal={false}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) =>
                        <VTComments/>
                        }
                    />
                </ScrollView>
            </View>

        );
    }
}
