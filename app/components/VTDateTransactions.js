/**
 * Created by DelaCerna on 2/13/17.
 */
import React, {Component} from 'react';
import {AppRegistry, View, ToastAndroid, Text, StyleSheet, Alert, RefreshControl} from 'react-native';
import {Container, Content, List, Title, ListItem, Icon, Left, Right, Header, Body} from 'native-base';
import {Button} from 'native-base';
import {Actions} from "react-native-router-flux";
import VTTransactionList from './VTTransactionList';
import VTTransactionListTourist from './VTTransactionListTourist';
let sessionUserData, userName, userNo, mqttServer;
import CacheStore from 'react-native-cache-store';
let next_page_url, prev_page_url;
export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false, tours: ''
        };
    }

    componentDidMount() {
        if (this.props.sessionUserData.UserTypeNo == 1)
            var url = this.props.databaseServer + '/get/tours/tourist/transaction/user/' + this.props.sessionUserData.UserNo
        else
            var url = this.props.databaseServer + '/get/tours/guide/transaction/user/' + this.props.sessionUserData.UserNo
        fetch(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json()
        })
            .then((responseData) => {
                CacheStore.set('tours', responseData);
                this.setState({tours: responseData})
            })
            .catch(function (error) {
                CacheStore.get('tours').then((value) => {
                    this.setState({tours: value})
                })
                throw error;
            });
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.fetchData()
    }

    render() {
        next_page_url = this.state.tours.next_page_url;
        prev_page_url = this.state.tours.prev_page_url;
        sessionUserData = this.props.sessionUserData
        mqttServer = this.props.mqttServer
        var counter = -1;
        return (
            <Container style={{backgroundColor:'#f8f8f8', padding:5}}>
                <Content refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }>
                    <Header style={{backgroundColor:'#ee09df'}}>
                        <Left>
                            { prev_page_url == null ? null :
                                <Button transparent onPress={()=>this.previousPage()}>
                                    <Icon name='ios-arrow-back'/>
                                </Button>
                            }
                        </Left>
                        <Body>
                        <Title>Page: {JSON.stringify(this.state.tours.current_page)}</Title>
                        </Body>
                        <Right>
                            {next_page_url == null ? null :
                                <Button transparent onPress={()=>this.nextPage()}>
                                    <Icon name='ios-arrow-forward'/>
                                </Button>
                            }
                        </Right>
                    </Header>
                    <List dataArray={this.state.tours.data} renderRow={(data) =>
                    <ListItem>
                        <VTTransactionList
                        databaseServer={this.props.databaseServer}
                        sessionUserData={this.props.sessionUserData}
                        mqttServer={this.props.mqttServer}
                        tours = {this.state.tours.data}
                        data = {data}
                        counter = {counter=counter+1}/>
                        </ListItem>
                    }
                    />
                </Content>
            </Container>
        );
    }

    fetchData() {
        CacheStore.get('tours').then((value) => {
            this.setState({tours: value})
        })
        fetch(this.props.databaseServer + '/get/tours/guide/' + this.props.sessionUserData.UserNo, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                CacheStore.set('tours', responseData);
                this.setState({tours: responseData, refreshing: false})
            })
            .catch(function (error) {
                // ToastAndroid.show('mySession:' + JSON.stringify(this.state.tours), ToastAndroid.SHORT)
                Alert.alert(
                    'Error!',
                    'Can\'t connect to to server! \n Make sure you have internet connections!',
                    [
                        {
                            text: 'OK!', onPress: () => console.log("OK")
                        },
                    ],
                    {cancelable: false}
                )
                throw error;
            });
    }

    previousPage() {
        this.setState({loading: true})
        fetch(prev_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                console.log(JSON.stringify(responseData))
                this.setState({tours: responseData})
            })
            .catch(function (error) {
                throw error;
            });
    }

    nextPage() {
        this.setState({loading: true})
        fetch(next_page_url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then((response) => {
            return response.json() // << This is the problem
        })
            .then((responseData) => { // responseData = undefined
                console.log(JSON.stringify(responseData))
                this.setState({tours: responseData})
            })
            .catch(function (error) {
                throw error;
            });
    }
};


