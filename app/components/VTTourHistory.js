/**
 * Created by DelaCerna on 1/25/17.
 */
'use strict';
import React, {Component} from 'react';
import {Image, View, Dimensions, Alert, BackAndroid, ToastAndroid} from 'react-native';
import {Button, Text, Card, CardItem, Left, Body, Container, Content, Icon, Right} from 'native-base';
const {width, height} = Dimensions.get('window');
import {Actions} from "react-native-router-flux";
let TourNo, RankDesc, mqttServer, Username, DestinationName, DestinationDesc, CategoryName, Rank, Price;
export default class extends Component {
    constructor() {
        super()
        this.state = {
            testing: '', myData: '', comments: 3
        }
    }

    render() {
        return (
            <Container>
                <Content>
                    <Card>
                        <CardItem>
                            <View style={{width:width*.75}}>
                                <Left>
                                    <Button onPress={()=>this.checkRanked('s', 's')}
                                            style={{justifyContent:'center',backgroundColor:'yellow',width: 60, height: 60, borderRadius: 30}}>
                                        <Text
                                            style={{alignSelf:'center', color: 'red', fontWeight: 'bold', fontSize: 25}}>Rank</Text>
                                    </Button>
                                    <Body>
                                    <Text>
                                        DestinationName
                                    </Text>
                                    <Text note>
                                        Username
                                    </Text>
                                    </Body>
                                </Left>
                            </View>
                        </CardItem>
                        <View style={{width:width*.75}}>
                            <CardItem content>

                                <Text>
                                    DestinationDesc
                                </Text>
                            </CardItem>
                        </View>
                        <CardItem>
                            <Left>
                                <Text>CategoryName</Text>
                            </Left>
                            <Text>Pricephp/hr</Text>
                            <Right>
                                <Button bordered>
                                    <Text>details</Text>
                                </Button>
                            </Right>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}